1) Para que el stock llegue a 0 se necesita al menos un consumidor que consuma lo que produce el productor, en el caso de mi trabajo, se producen numeros aleatorios, por lo tanto no es un numero fijo la cantidad de consumidores. Va variando de acuerdo a los numeros que se aplican.

2) El metodo sinchronized limita el acceso a un m�todo u objeto de un thread cada vez. Este tipo de metodo fue utilizado en los metodos "producir" y "consumir" para limitar el acceso a estos.

3) Elrecurso compartido tiene la funcion de ocupar el constructor de cada hilo, donde se va a realizar los metodos que contiene este recurso. Tiene una variable stock que es el que determina la cantidad de cerveza que se produce y consume

4) Una de las formas para instanciar un Thread es haciendo un new en el main. Ejemplo: Thread nombre_thread = new Thread(instancia_de_clase); Otra forma de instanciar un hilo es que una clase extienda de la clase Thread. Ejemplo: class nombre_de_la_clase extends Thread{ } Por ultimo podemos utilizar un thread implementando una interface llamada runnable en nuestra clase. Ejemplo: class nombre_de_la_clase implements Runnable{ }
