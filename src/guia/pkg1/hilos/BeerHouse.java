/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */   
   
package guia.pkg1.hilos;

import java.util.logging.Level;
import java.util.logging.Logger;

public class BeerHouse{
        
    public int stock = 100;
        
    public synchronized int consumir(int numero){
            
        while(this.stock <= 0){
            try {
                wait();        
            } catch (InterruptedException ex) {
                System.out.println("Error en el beerhouse");
            }
        }
        
        if(numero > this.stock){
            numero = this.stock;
        }
        this.stock = this.stock - numero;
        notify();
        
        System.out.println("EL stock quedo en " + this.stock);
        return numero;
    }
  
    public synchronized void producir(int numero){

        while(this.stock >= 100){
            try {
                wait();
            } catch (InterruptedException ex) {
                System.out.println("Error en el beerhouse");
            }
        }
        
        this.stock += numero;
        notify();
    }
}
