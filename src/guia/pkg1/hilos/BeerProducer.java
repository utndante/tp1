/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guia.pkg1.hilos;

import java.util.List;

public class BeerProducer extends Thread{
    
    //List <String> beer;
    private final BeerHouse bh;

    public BeerProducer(BeerHouse bh) {
        this.bh = bh;
    }
    
    @Override
    public void run() {
    
        while(true){
            int cant = (int)(Math.random()*100) + 1;
            
            System.out.println("Estoy produciendo  " + cant); 
            this.bh.producir(cant);
            
            try{
                Thread.sleep(1000);
            }
            catch (InterruptedException e) {
                System.out.println("Error en el thread");
            }
        }   
    }    
}   
    
