/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guia.pkg1.hilos;

/**
 *
 * @author Dante
 */
public class Main {

    public static void main(String[] args) {
       
        System.out.println("Creando cervezeria");
        BeerHouse cerveceria = new BeerHouse();
        
        System.out.println("Creando productor");
        BeerProducer producer = new BeerProducer(cerveceria);
        
        System.out.println("Creando consumidor");
        BeerConsumer consumer = new BeerConsumer(cerveceria);
        producer.start();
        consumer.start();
    }
    
}
